# Visualization and analytical software tools: Lectures notes 

## Lecture 1 
**Requirements**:  Now you see it from Stephen Few

**Definitions**: The use of computer-supported, interactive, visual representations of abstract data to amplify cognition.

The immediate purpose is to understand the data to at the end take the good decisions

*Tableau* is a really powerful and fast software for visualization

There is a lot of visualization tool.

>The best software for (visual) data analysis is the one you forget you are using. *Stephen Few*

*Gartner* Magic Quadrant for Analytics and BI platforms:
![image](magicquadrant.png)

**Leaders now**: 
- Power BI
- Tableau

Tableau limitations are mainly related to avoid manipulate visualization

**Some important characteristics of idal data**:
- High volume:  More information imply more chance we have what we need
- Historical: Much insight with information about it has change
- Consistent: Things change over time, when they do, it's helpful to keep data consistent
- Multivariate: Quantitative, categorical, ...
- Atomic:  Data at the lowest level you want to analyze.
- Clean (Garbage In Garbage Out): The quality of our analysis can never exceed the quality of our information.
- Clear: Understanding the data is important, name should be meaningful
- Dimensionally Structured:  SQL Queries is not analysis; it is a task before processing analysis
- Richly segmented: Making meaningful groups can make benefits of a lot of analysis
- Of known Pedigree: Integrity is important, it is important to know where the information come from, and what can have been used to create it

## Lecture 2: Graphical excellence and integrity

### Graphical excellence

Well designed presentation of interesting data.

**Characterized by**: 
- Correct and well-prepared data
- solid analysis
- Well-designed plot
- Clarity, precision, efficiency

**Benefits**:
- Greatest number of ideas
- In the shortest time
- With the least ink (as less distraction as possible)
- In the smallest space

### Graphical integrity

Visualization can distort the information, making it hard for the viewer to learn the truth.

**We are surrounded with deceptive visualizations** 

Most of us have potential to discern them

**Distorsions, inconsistency**: 
To avoid this, directly proportional to present quantitative data.

Lie Factor, LF =  Relative change shown in visualization/ Relative change in data

Acceptable LF is 0.95 to 1.05
Overstating LF > 1, Understating LF < 1

- Visual representation of measures, perception change with experiences, depends on the context, a particular problem is changing more dimensions at the same time

**Showing 1D data in 2D or 3D, should be avoided !**

**Recommandations**
- Direct proportional (of geometric elements)
- Clearly labeled
- Data variation is deplayed (not design variation)
- Keep number of dimensions
- Show data only inside the context

### How to gauge the quality of a visualization ?
- It show you a problem with your data
- Each successfull visualization leads to the long conversations
- It is not a video game, it should not be fun to make, so when it is starting to seem like this, STOP
- Watch someone else deal with your visualization (Do they see the insight or not)

Based on Lartin Wattenberg

## Data Quality & Usability hurdles
- Missing data
- Erroreneous Values
- Type conversion
- Entity Resolution
- Data integration

You should anticipate problems with your data !


## Interactivity in visualisation

Good visualization lead to question, and you will be glad there is interaction

Interactions:
- Comparing 
- Sorting
- Add vars
- Filtering
- Highlighting
- Aggregating

Advanced:
- Re-expressing
- Re-visualizing
- Zooming and panning
- Re-scaling
- Accessing details on demand 
- Annotating
- Bookmarking

Reading: page 55-82

### Comparing
When we compare data: we want either to look for similarities (comparing) but also differences (contrasting)

basic:
we compare value of one variable: magnitude
we can also compare the patterns of time series or distribution

Many data analysis product fails to fully support comparisons of distrib because they forgot to include box-plot (boite a moustache) and because of hardly readable object (3d graph for example)

- Ranking comparison: Order is important
- part to whole: Better than pie char
- Deviation comparison: differences of two sets of values
- Time series (points or curve) comparison

### Sorting
Simple but powerful

Combination of Sorting + comparing can be really powerful

When we add another variable to sorted graph we can see if there is a correlation easily

**Visu soft should allow**:  sorting quickly on multiple thing, sorting multiple graph linked by a same categorical variable in the same way

### Adding variables
We can add measure for example (like sub category)

**We can combine value to solve issue with sorting because of same name between categories, will concat subcat name with cat name** 

### Filtering
Reducing the view of a subset of data

**Purpose**: Remove anything unuseful in this context, make visu more clear

Filters control like radio buttons or sliders are really great to allow fast changes

**Visu soft should allow**:  filtering with fast controls, finltering multiple graph linked in the same way in one action, allow filtering even on variables not shown directly on the graph, have a reminder of the filter applied on the visu

### Highlighting

Keep the context but help to clearly see the selected dimension

Really useful with scatter plot

**Visu soft should allow**:  Highlighting quickly, hglting by range or value of vars, hglting by click or select on graph, hglted points to be highlitghted directly on others linked graph

### Aggregating

We don't change amount of data

**Visu soft should allow**: agregating easily and with mutliple functions (sum, average, means, etc), agregating and opposite without noticeable delay, create adhoc grouping easily

#### Drilling
Drill-up =  Aggregation (like sum, average, ...)
Drill-down = See repartition (sub-level)

**Visu soft should allow**: provide way to define hierarchy among categorical relationship, ability to skip level when drilling, auto define of time hierarchy (month, day, hours, quarters, years, etc.)

### Re-expressing
Write quantitive var in another way, (value to percent scale or using a reference value (first one for example in time series))

We can do both, making value based on average value of neighbors

**Visu soft should allow to things explained over quickly**

### Re-visualizing
Some way, can allow people to direcly be able to answer their questions. No visualization is perfect to answer all analytical, we should switch depending of what we want.

**Visu soft should allow**: Quick switch between visu types, provide list of graph adapted to this visu, prevent or make difficult use of unappropriate graph type (which would display the data in inappropriate way)

### Zooming and panning
Zooming and be able to move left and right once zoomed
**Visu soft should allow to do this easily**
### Re-scaling
Using multiple scale (linear, log10), can help to show rate of increasing (really different from value increase)
**Visu soft should allow**:change scale to log (and able to go back) and choose the base, provide mean to select start and end value to the scale, prevent miss reading because of non linearity when using log scale.
### Accessing details on demand 
Give info with tooltips for example (can even put chart in tooltip in Tableau)
**Visu soft should allow tooltips like info**

### Annotating 
Static annotation of important info related to graph

Dynamic annotation: When I remove points, the annotation adapt to move the elements they are linked

We could also put it staticly at a location on a screen
**Visu soft should allow annotation allowing the over features**

### Bookmarking
Stories : it is like a powerpoint presentation in which you can save particular view (filter, sorts, everything), and make a slide with them which stay interactive

**Visu soft should allow**: Way to save current state of analysis easily, maintain history of steps and states of analysis to make easy to go back to a previous state, and provide a way to access this history to easily choose the state we want to go back to

## Analytical navigation
High level overview of best practices 

Data analysis, like experimentation, must be considered as an open-minded, highly interactive, iterative process,..

- Directed: Begins with a specific question we want to answer
- Exploratory: Simply looking at data at beginning, try looking something interesting

**Shneiderman's mantra**: Overview -> Zoom&Filter -> Details-on-demand

Overview: 
- Reduces Search
- Allows detection of overall patterns
- Aids the user in choosing the next move

Zoom & Filter: 
- Iterative narrowing of the focus
- Removing irrelevant information

Details On-Demand:
- Drill-down to details

This way works great. It is like the CIA techniques. We should not look like for something since the beginning, exploratory part is important (difference between directed and exploratory).


## Hierarchical navigation
- Node-link visualization: Clear but too much data can make it difficult to read on a single screen
- Treemaps: Invented by Shneiderman,  to reduce drawbacks of node-link. 

Treemaps visu:
- Display two quantitative variable at the same time: Size and Color
- Use containment to show relationship

## Analytical patterns
Can help us to see knowledge of the data

- Quantitative analysis involves examining relationships among values

- Example of relationship types: Time-series, Ranking and Part-To-Whole, Deviation, Distribution, Correlation, Multivariate

When data is shown in the goodway we distinct patterns like Verbal language: 

Basic units:  Points, lines, bars and boxes

We combine them to: Reveal quantitative meaning... according to rules of preception (to make people able to see it at first sight)

**Basic units**:

Bars: 
- *Properties*:  Difference in heights, separation
- *Purpose*: Display differences in magnitude, emphasize individuality

Boxes: 
- *Properties*: Don't share a common baseline, tend to notice: differences between the position of their tops and bottoms, differences between horizontal lines,.
- *Purpose*: Distribution of data (box plot)

Lines:
- *Properties*: Emphasize overall trend and specific patterns of change in a set of values
- *Purpose*: Show the shape of change from one value to the next, especially through time (raise/fall/stability)

Points: 
- *Properties*: Simultaneously encoding of quantitative values (horizontal and vertical position)
- *Purpose*:  They can substitue bar graphs (avoiding useless part (all graphs have almost same values), and also avoiding using not zero starting which lead to misinterpretation), we can see correlation, mark positions of values along lines.

### In the Smooth and in the Rough
**In the Smooth**: Other term for fit, line or curve can represent it, show general shape of the data. We use it to describe the overall pattern of the data.

**In the Rough**: Abnormal values in a set of data.

- Exceptions:  Data that falls outside defined standards, expectations, or any other definition of normal

- Outliers: Statistical term to refer to values that fall outside the norm based on a stastical calculation, 3 time std from mean.

Outliers can be outside of norm because of:
- Errors: Issue with data
- Extraordinary event: Storm for example
- Extraordinary entities: Behaviour not expected 

We want spotting exceptions to be easy as possible, depending of dataset can not be categorized in same way (smooth, extreme, outlier).

One of the great benefits of information visualization is the opportunity to offload cognitive workload to overver's eyes.

We should not waste the observer's brains on activities that their eyes can do faster and with much less effort.

We can use ranges of normal with lines, shadings (they can have different shape and angle)

Acceptable ranges: single line, shading once again 

### Pattern examples:
There is an infinite of unique visual patterns in the world but the number of patterns that represent meaningfull quantitave info in 2D graphs is limited. 

If we learn to recognize the most meaningfull patterns in our data, we will be able to spot them faster and more often, which will save us time and effort.

Our ability to perceive is remarkable but limited by working memory.

Pattern examples:
- Low, High, in between
- Going up, Going down, Remaining flat
- Intersecting, Non-intersecting
- Wide, narrow of curve
- Clusters, Gaps of points
- Tightly distributed, Loosely distributed
- Normal, Abnormal
- Symmetrical, Skewed
- Random, Repeating
- Straight, Curved
- Steep, Gradual
- etc.

Humans pattern recognition skills are amazing, and sources of great insights but sometimes so good that we recognize ones that are not really there.
Even when a pattern is real, we often err by ceasing our explanation once we see a pattern we know, moreover trained to see it, but we miss others that are unfamiliar and unexpected.

**We should never become so good at spotting patterns that we lose sight of the info that composes them !**

View data with fresh eyes, empty mind, without biases, look without preconceptions, only empty minded we can see something new. When new and unexpected forms and meanings appear, that help us to answer the question, "What we don't know, we don't know ?"
## Notes

- Bar graphs should always start from zero
- Avoid Pie chart and even more 3D pie chart
- Misleading time perception
- Use line charts (almost) only with time series 
- Time sequence should be expected to be conform according to the environment
- Vertically oriented text is hard to read
- DO not use reflection Transparency, things like this, overlapping 
- Sort the data
- Scatter plot: Correlation
- Bar plot: Distribution

